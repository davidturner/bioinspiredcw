import argparse
from ANN import ArtificialNeuralNetwork
from PSO import ParticleSwarmOptimiser, vector_to_network, create_rand_position
from tqdm import tqdm
from activations import activations
from utils import load_data_from_file


def argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--layers', type=int,
                        help='Number of hidden layers.', required=True)
    parser.add_argument(
        '-a', '--activation', help=f"Activation for each Neuron choice of Sigmoid, tanh, cos, gaussian. If ommited the default will be sigmoid. \n \
            {pp_activations}")
    parser.add_argument(
        '-f', '--file', help='Path to the data location. Expected <../../<filename>.txt>', required=True)
    parser.add_argument('-v', '--visualisation',
                        help='"y" or "n" value to turn visualisation of the data and network on or off. Default is "n".')
    parser.add_argument(
        '-o', '--optimizer', help='Use Backpropagation to optimise the network or Particle swarm optimisation')
    return parser.parse_args()


if __name__ == '__main__':
    pp_activations = '\n'
    tmp = []
    for i in activations:
        tmp.append(f'{i}: {activations[i]()}')
    pp_activations = pp_activations.join(tmp)
    args = argparser()

    layers = []

    for layer in range(args.layers):
        var = None
        while var is None:

            try:
                var = int(
                    input(f'How many Nodes in hidden layer {layer + 1}?  '))
                layers.append(var)
            except Exception as e:
                print(f'{e} please try again')

    if args.activation is None:
        a = activations[1]()
    else:
        a = activations[int(args.activation)]()

    input_d, target = load_data_from_file(args.file)

    network = ArtificialNeuralNetwork(layers, file=args.file, activation=a,
                                      input_data=input_d, target_outputs=target,
                                      weights=True, optimiser='pso', epochs=50)
    network.train()

    if args.visualisation == 'y':
        network.plot_error()
        network.plot_data()
        # network.network_graph()
