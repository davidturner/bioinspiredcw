from ANN import ArtificialNeuralNetwork
from activations import activations
from utils import load_data_from_file
import matplotlib.pyplot as plt

files = ['Data/1in_linear.txt', 'Data/1in_cubic.txt', 'Data/1in_sine.txt',
         'Data/1in_tanh.txt', 'Data/2in_complex.txt', 'Data/2in_xor.txt']

number_of_subgroups = [1, 2, 3, 4, 5, 6]
number_of_particles = [10, 15, 20, 25, 30]
number_of_hidden_layers = [1, 2]
number_of_neurons = [1, 2, 3, 4, 5]
activations = activations

for file in files:
    input_data, target_outputs = load_data_from_file(file)
    # Create a network with the varying structures subgroups, particles, hidden layers
    # neurons per layer and activations
    for i in range(len(activations)):
        for group in number_of_subgroups:
            for layer in number_of_hidden_layers:
                for neuron in number_of_neurons:
                    l = [neuron for _ in range(layer)]
                    network = ArtificialNeuralNetwork(
                        l, file, input_data, target_outputs, activation=activations[i](), epochs=50, optimiser='pso', pso_sub_groups=group)
                    network.train()
                    network.plot_data(
                        save=True, filename=f'eval/{network.file.split("/")[-1].split(".")[0]}/network_{"-".join([str(x) for x in network.network_shape])}_optimizer{network.optimiser}_optimizergroups{network.pso_sub_groups}_activation{network.activation}_datavis.png')
                    network.plot_pso_g_best_error_over_time(
                        save=True, filename=f'eval/{network.file.split("/")[-1].split(".")[0]}/network_{"-".join([str(x) for x in network.network_shape])}_optimizer{network.optimiser}_optimizergroups{network.pso_sub_groups}_activation{network.activation}_psocostovertime.png')
