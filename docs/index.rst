Coursework Documentation
=========================

.. mdinclude::
    ../README.md

.. image:: ../network_topology.gv-1.jpg
    :width: 600
    :alt: Example Network Structure

.. toctree::
   :maxdepth: 4
   :caption: Contents

   ANN
   PSO
   activations
   utils
