import numpy as np
import random
from utils import vector_to_network, create_rand_position
from tqdm import tqdm


class ParticleSwarmOptimiser:

    def __init__(self, pop_size: int, iterations: int, dimensions: list,
                 min_val: float, max_val: float, n_sub_groups: int, network):
        """
        An optimiser for the Artificial Neural Network Class using the Particle Swarm approach to find the optimal solution.

        :param pop_size: The size of the Swarm population
        :type pop_size: int
        :param iterations: The number of iterations for the search.
        :type iterations: int
        :param dimensions: The dimensions of the search space
        :type dimensions: int
        :param min_val: The minimum initial value of the search space at each dimension
        :type min_val: float
        :param max_val: The maximum initial value of the search space at each dimension
        :type max_val: float
        :param n_sub_groups: The number of social groups that will inform the search of each particle in a sub group
        :type n_sub_groups: int
        """

        """
        TODO: random sample the number of particles in the list
        """
        total_dims = 0
        for x in range(1, len(dimensions)):
            total_dims += dimensions[x-1] * dimensions[x]

        self.pop_size = pop_size
        self.n_sub_groups = n_sub_groups
        self.sub_group_crossover = 2
        self.dimensions = total_dims
        self.particles = []
        self.iterations = iterations
        self.g_best_cost = float('inf')
        self.g_best_pos = create_rand_position(
            self.dimensions, min_val, max_val)
        self.subgroup_best_cost = [float('inf') for _ in range(n_sub_groups)]
        self.subgroup_best_pos = [create_rand_position(
            self.dimensions, min_val, max_val) for _ in range(n_sub_groups)]
        self.create_particles()
        self.group_split = []
        self.network_to_optimise = network

    def __str__(self):
        return f'Global Best Position:  {self.g_best_pos} \n \
                 Global Best Cost:      {self.g_best_cost}'

    def divide_list_into_groups(self):
        """divide_list_into_groups"""

        p_copy = np.arange(0, len(self.particles))

        random.shuffle(p_copy)
        split = list(range(self.n_sub_groups))
        if len(p_copy) % self.n_sub_groups != 0:
            increment = int(round((len(p_copy)/self.n_sub_groups), 0))
            position = 0
            for i in range(self.n_sub_groups):
                if i == 0:
                    split[i] = p_copy[:increment]
                    position += increment
                elif i == self.n_sub_groups - 1:
                    split[i] = p_copy[position:]
                    position += increment
                else:
                    split[i] = p_copy[position:position + increment]
                    position += increment
        else:
            split = np.split(p_copy, self.n_sub_groups)

        for i in range(self.n_sub_groups):
            # This is when a particle will be assigned to a group and
            # the particles current best will be assigned to group best if
            # it is better than the current

            print(
                f"Group id: {i}, Length of Group: {len(split[i])}, Length of Population {len(p_copy)} ")
            for p in range(len(split[i])):
                self.particles[split[i][p]].sub_group_id = i

                # self.particles[p[0]].sub_group_id = i
            # self.group_split.extend([(x, i) for x in split[i]])

    def create_particles(self):
        """create_particles"""
        for _ in range(self.pop_size):
            self.particles.append(Particle(self.dimensions, -1, 1, self))
        self.divide_list_into_groups()

    def train(self):
        """train"""
        progress = tqdm(range(self.iterations))
        for _ in progress:
            for particle in self.particles:
                particle.update_velocity()
                particle.update_position()
                particle.evaluate()
                particle.set_weights()
                progress.set_description(
                    f"G_B MSE: {round(self.g_best_cost, 8)}")
        # plot the gloabl best solution here
        vector_to_network(self.g_best_pos, self.network_to_optimise)
        self.network_to_optimise.forward_pass()

    def plot_cost(self):
        pass


class Particle:

    """
        This class will take the list of desired outputs a full pass of the
        network with the weights that are currently calculated. Desired
        output and actual output pairs will be created and then sent
        through the mean squared error cost function to minimise the error.
    """

    def __init__(self, dimension: int, min_val: float, max_val: float, optimiser: ParticleSwarmOptimiser, weights: list = [None]):
        """

        :param dimension: The dimensionality of the particles search space
        :type dimension: int
        :param min_val: The minimum value of the initial value of the search space location at each dimension
        :type min_val: float
        :param max_val: The maximum value of the initial value of the search space location at each dimension
        :type max_val: float
        :param weights: Accepts a list of weights if the particle is being initialised from a certain position, defaults to [None]
        :type weights: list , optional
        """

        self.sub_group_id = None
        self.dimension = dimension
        self.position = create_rand_position(dimension, -1, 1)
        self.velocity = create_rand_position(dimension, -1, 1)
        self.current_cost = float('inf')
        self.p_best_cost = float('inf')
        self.p_best_position = self.position
        self.pso = optimiser

    def set_weights(self):
        """
        Converts the weights represented in the particles position back to the structure of the network.
        """
        vector_to_network(self.position, self.pso.network_to_optimise)

    def get_weights(self):
        """
        Retrieves the weights from the neural network in a 1D array and sets this as the current position of the particle.
        """
        self.position = self.pso.network_to_optimise.to_vector()

    def __str__(self):
        return f'Current position is:   {self.position} \n \
                    Current Cost is:    {self.current_cost} \n \
                    Sub group id is:    {self.sub_group_id}'

    def evaluate(self):
        """
        This is where a forward pass will be made on the neural network. First Step is to convert the weight vector back
        into the neural network. Second is to perform the forward pass. Third calculate mean squared error. Fourth
        update cost. if new cost is < current cost then update personal best cost and position. if new cost is better
        than the particle subgroup best then update. with this cost and the current weight vector
        """
        vector_to_network(self.position, self.pso.network_to_optimise)
        self.pso.network_to_optimise.forward_pass()
        self.current_cost = self.pso.network_to_optimise.ms_error
        self.position = self.pso.network_to_optimise.to_vector()

        if self.current_cost < self. p_best_cost:
            self.p_best_position = self.position
            self.p_best_cost = self.current_cost
        if self.current_cost < self.pso.g_best_cost:
            self.pso.g_best_pos = self.position
            self.pso.g_best_cost = self.current_cost
        if self.current_cost < self.pso.subgroup_best_cost[self.sub_group_id]:
            self.pso.subgroup_best_pos[self.sub_group_id] = self.position
            self.pso.subgroup_best_cost[self.sub_group_id] = self.current_cost

    def update_velocity(self):
        """
        Method that will calculate the updates in velocity of each weight in the neural network based on the global
        best, the personal best and the social group best.
        Calculated using the formula:

        .. math::
            v_i(t+1) = wv_i(t)+c_1r_1[\\hat{x}_i(t)-x_i(t)]+c_2r_2[g(t)-x_i(t)]

        Where w is a constant, i is the index, c1 and c2 are personal and global constants determining the accelleration
        towards the personal best and the global best, r1 and r2 are randomly generated numbers between 0 and 1.
        x_i hat is the current best at index, g is the global best

        Expanded that is:
        calculate personal velocity update
        c1 * rand1 * (personal best - current)

        calculate global velocity update
        c2 * rand2 * (global best - current)

        calculate social velocity update
        c3 * rand3 * (social best - current)

        velocity update
        velocity of dimension = w * old velocity + personal_update + gloabl_update + social update

        """

        w = 0.5
        c1 = 1
        c2 = 2
        c3 = 1

        for i in range(len(self.position)):
            r1 = random.random()
            r2 = random.random()
            r3 = random.random()

            personal_velocity_update = c1 * r1 * \
                (self.p_best_position[i] - self.position[i])
            gloabl_velocity_update = c2 * r2 * \
                (self.pso.g_best_pos[i] - self.position[i])
            social_velocity_update = c3 * r3 * \
                (self.pso.subgroup_best_pos[self.sub_group_id]
                 [i] - self.position[i])

            self.velocity[i] = w * self.velocity[i] + \
                personal_velocity_update + gloabl_velocity_update + social_velocity_update

    def update_position(self):
        """
        Update the position of each dimension in the search space.
        """

        for i in range(len(self.position)):
            self.position[i] = self.position[i] + self.velocity[i]

            # Impliment negative bounds and upper bounds
            # if self.position[i] > 10:
            #     self.position[i] = 10
            # if self.position[i] < -10:
            #     self.position[i] = -10


if __name__ == '__main__':
    particles = [x for x in range(20)]
    print(particles)
    print(random.sample(particles, 6))
    print(random.sample(particles, 6))

    # test_array = np.asarray(
    #     [2, 4, 2])

    # ann = ArtificialNeuralNetwork([3, 3], file='Data/1in_linear.txt')

    # pso = ParticleSwarmOptimiser(23, 20, test_array, -0.3, 0.3, n_sub_groups=2)

    # pso.particles[0].update_velocity()

'''
For network optimisation:
    set weights from particle swarm random initialisation
    one iteration of a data point
    calculate error
    apply pso formula
    update weights reapply data point
    calculate error
    continue

USAGE:
    option for either batch or online learning


'''
