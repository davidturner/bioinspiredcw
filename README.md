# Bio inspired coursework

## Usage

    usage: main.py [-h] -l LAYERS [-a ACTIVATION] -f FILE [-v VISUALISATION]
               [-o OPTIMIZER]

optional arguments:
  -h, --help            show this help message and exit
  -l LAYERS, --layers LAYERS
                        Number of hidden layers.
  -a ACTIVATION, --activation ACTIVATION
                        Activation for each Neuron choice of Sigmoid, tanh,
                        cos, gaussian. If ommited the default will be sigmoid.
                        0: Null 1: Sigmoid 2: TanH 3: cos 4: Gaussian 5: ReLU
                        6: Leaky ReLU 7: Identity
  -f FILE, --file FILE  Path to the data location. Expected
                        <../../<filename>.txt>
  -v VISUALISATION, --visualisation VISUALISATION
                        "y" or "n" value to turn visualisation of the data and
                        network on or off. Default is "n".
  -o OPTIMIZER, --optimizer OPTIMIZER
                        Use Backpropagation to optimise the network or
                        Particle swarm optimisation

Upon program start you will be asked for the number of nodes in each hidden layer

## Neural Network

Example Structure:

![Image](https://gitlab.com/davidturner/bioinspiredcw/raw/master/network_topology.gv-1.jpg)

## TODO

    Test Hyperparams
    Comment code
    Writeup

