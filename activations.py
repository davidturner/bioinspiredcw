import numpy as np


class Sigmoid:
    '''
    Sigmoid Activation function impliments activation and derivative methods.
    '''

    def __str__(self):
        return('Sigmoid')

    def activation(self, value):
        '''
        impliments sigmoid activation.
        .. math::
            1/(1 + e^-x)
        '''
        return 1/(1 + np.exp(-value))

    def derivative(self, value):
        return value * (1.0 - value)


class TanH:

    def __str__(self):
        return('TanH')

    def activation(self, value):
        return np.tanh(value)

    def derivative(self, value):
        return 1 - (value**2)


class Cos:

    def __str__(self):
        return('cos')

    def activation(self, value):
        return np.cos(value)

    def derivative(self, value):
        return -(np.sin(value))


class Gaussian:

    def __str__(self):
        return('Gaussian')

    def activation(self, value):
        return np.exp(-((np.square(value)/2)))

    def derivative(self, value):
        return -2 * value * np.exp(-value**2)


class ReLU:

    def __str__(self):
        return('ReLU')

    def activation(self, value):
        return np.float64(max(0.0, value))

    def derivative(self, value):
        return 1 if value > 0 else 0


class LReLU:

    def __str__(self):
        return('Leaky ReLU')

    def activation(self, value):
        return np.float64(max(0.01*value, value))

    def derivative(self, value):
        return 1 if value >= 0 else 0.04


class Identity:

    def __str__(self):
        return('Identity')

    def activation(self, value):
        return value

    def derivative(self, value):
        return 1


class Null:

    def __str__(self):
        return('Null')

    def activation(self, value):
        return 0

    def derivative(self, value):
        return 0


activations = {
    0: Null,
    1: Sigmoid,
    2: TanH,
    3: Cos,
    4: Gaussian,
    5: ReLU,
    6: LReLU,
    7: Identity,
}

if __name__ == '__main__':
    print(activations)
